'use strict';

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
function userInputChecker() {
  //se crea un bucle infinito que comprobará una y otra vez que el valor introducido
  //es correcto, cuando se cumplan las condiciones se rompe con el return
  do {
    let userInput = Number(prompt('Introduce un número entero entre 0 y 100'));
    if (isNaN(userInput) || userInput == '') {
      alert('No es un número, intentelo de nuevo.');
    } else if (userInput > 100 || userInput < 0) {
      alert('El número no está entre 0 y 100, intentelo de nuevo.');
    } else {
      return userInput;
    }
  } while (true);
}
function higherLower(input, password) {
  //simplemente se compara la contraseña y el input para indicar si es mayor o menor
  if (password > input) {
    return 'La contraseña es un número mayor al introducido.';
  } else {
    return 'La contraseña es un número menor al introducido.';
  }
}
function pluralOrSingular(numOfOportunities) {
  //se comprueba si la respuesta debe estar en plural o singular
  if (numOfOportunities === 1) {
    return `Has fallado, ${numOfOportunities} oportunidad restante.`;
  } else if (numOfOportunities > 1) {
    return `Has fallado, ${numOfOportunities} oportunidades restantes.`;
  } else {
    return `TE HAS QUEDADO SIN OPORTUNIDADES!!`;
  }
}
(function passwordGame() {
  //se combina todo lo anterior y se realiza el juego
  let numTries = 5;
  let isUserRight = false;
  let passwordGenerated = getRandomInt(0, 100);
  //dejo la consola para comprobar si al acertar todo funciona bien
  //no me he olvidado de ella :)
  console.log(passwordGenerated);
  do {
    let userResponse = userInputChecker();
    if (userResponse === passwordGenerated) {
      isUserRight = true;
    } else {
      numTries--;
      alert(`${pluralOrSingular(numTries)}`);
      //se añade este if para que cuando se pierda no indique si el número introducido
      //era mayor o menor, ya que se perdio
      if (numTries > 0) {
        alert(`${higherLower(userResponse, passwordGenerated)}`);
      }
    }
  } while (!isUserRight && numTries > 0);
  //se comprueba la condición para el mensaje de victoria o derrota
  if (isUserRight) {
    alert('ENHORABUENA, HAS ACERTADO!!');
  } else {
    alert(`HAS PERDIDO!! LA CONTRASEÑA ERA ${passwordGenerated}!!`);
  }
  //de esta manera se puede reiniciar el juego si se desea sin necesidad de refrescar la página
  let newGame = confirm('¿Deseas jugar de nuevo?');
  if (newGame) {
    passwordGame();
  } else {
    alert('GRACIAS POR JUGAR!!');
  }
})();
