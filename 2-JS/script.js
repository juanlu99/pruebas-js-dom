'use strict';

const puntuaciones = [
  {
    equipo: 'Toros Negros',
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: 'Amanecer Dorado',
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: 'Águilas Plateadas',
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: 'Leones Carmesí',
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: 'Rosas Azules',
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: 'Mantis Verdes',
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: 'Ciervos Celestes',
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: 'Pavos Reales Coral',
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: 'Orcas Moradas',
    puntos: [2, 3, 3, 4],
  },
];

function arrayOfPointsGenerator(arrayOfTeams) {
  //Genero un array con las puntuaciones totales de cada equipo
  let arrayOfTotalPoints = [];
  for (const team of arrayOfTeams) {
    let totalPoints = 0;
    const { puntos } = team;
    for (const puntuacion of puntos) {
      totalPoints = totalPoints + puntuacion;
    }
    arrayOfTotalPoints.push(totalPoints);
  }
  return arrayOfTotalPoints;
}
function minAndMaxPointsTeams(arrayOfPoints) {
  //Obtengo el valor de la mayor y la menor puntuacion total
  let maxTotalPoints = Math.max(...arrayOfPoints);
  let minTotalPoints = Math.min(...arrayOfPoints);
  //Obtengo el index de las respectivas puntuaciones
  let indexOfMaxTotal = arrayOfPoints.indexOf(maxTotalPoints);
  let indexOfMinTotal = arrayOfPoints.indexOf(minTotalPoints);
  //Relaciono la posicion de la puntuacion total en el array con la posicion
  //del equipo en el array de equipos para obtener el nombre del mismo
  console.log(`Máximo anotador: ${puntuaciones[indexOfMaxTotal].equipo} con ${maxTotalPoints} puntos.`);
  console.log(`Mínimo anotador: ${puntuaciones[indexOfMinTotal].equipo} con ${minTotalPoints} puntos.`);
}
function maxAndMinTeller(arrayTeams) {
  //Junto las dos funciones en una sola
  let arrayOfTotalPoints = arrayOfPointsGenerator(arrayTeams);
  minAndMaxPointsTeams(arrayOfTotalPoints);
}

maxAndMinTeller(puntuaciones);
