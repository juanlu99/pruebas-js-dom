# Ejercicio JavaScript 2

Edita el archivo script.js para crear una función que reciba una array de objetos (equipos y puntos conseguidos) y muestre por consola el ** nombre ** del que más y del que menos puntos hayan conseguido, con sus respectivos ** totales **. (Totales del que más y el que menos unicamente)

Encontrarás un array de ejemplo en el propio documento.
