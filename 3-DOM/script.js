'use strict';

function getTime() {
  const now = new Date();
  const currentTime = now.toLocaleTimeString('es-ES', {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
  });
  return currentTime;
}

(function webClock() {
  const div = document.createElement('div');
  document.body.append(div);
  div.textContent = `${getTime()}`;
  setInterval(() => {
    div.textContent = `${getTime()}`;
  }, 1000);
})();
